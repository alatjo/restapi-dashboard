import React, { Component } from 'react';
import {connect} from 'react-redux';
import * as loginActions from '../../actions/login-actions';
import Base64 from 'base-64';
import * as Config from '../../resources/config';
import _ from 'lodash';
import './Login.css';

class Login extends Component{

  constructor(props){
    super(props);
    this.state = {openMenuBox: this.props.isLoginBoxOpen};
    this.openLoginBox = this.openLoginBox.bind(this);
    this.props.openLoginBox(this.openLoginBox);
  }

  openLoginBox(){
    this.props.toggleLoginBox();
    if(!this.props.loginState.loggedIn){
      const loginContent = document.getElementById("login-content");
      let loginMenu = document.getElementById("login-menu");
      loginContent.style.opacity = 0;
      if(!this.props.isLoginBoxOpen){
        loginMenu.addEventListener("transitionend", () => {
          loginContent.style.opacity = 1;
        }, false);
      }
    }
  }

  login(){
    let username = document.getElementById('username').value;
    let password = document.getElementById('password').value;
    const credentials = Base64.encode(username.concat(':', password));
    fetch(Config.backend.loginUrl, {
      headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Basic ' + credentials
      }
    })
    .then(this.setState({loadingSpinner:true}))
    .then((response) => {
      if(response.ok){
        const loginState = {
          loginState: {
            credentials: credentials,
            loggedIn: true
          }
        };
        console.log('success');
        this.setState({loggedIn:true, loginFailed:false, loadingSpinner:false});
        this.props.saveCredentials(loginState);
        this.openLoginBox();
        username = "";
        password = "";
      }else {
        console.log('fail');
      }
    })
    .catch((error) => {
      console.log('Login failed: '+error)
      this.setState({loginFailed:true, loadingSpinner:false})
    })
  }

  logout(){
    this.setState({
      loggedIn: false
    });
    this.props.saveCredentials({
      loginState: {
        credentials: '',
        loggedIn: false
      }
    });
    window.clearInterval(this.props.intervalId);
    this.openLoginBox();
    this.props.disconnectStompClient();
  }

  keyPress(e) {
    if(e && e.key === 'Enter') {
      this.login();
    }
  }

  componentDidUpdate(prevProps){
    if(!_.isEqual(this.props.loginState, prevProps.loginState)){
      this.setState({loginState: this.props.loginState});
    }
  }

  render(){
    return(
      <div className="login">
        <div className="login-icon" onClick={() => this.openLoginBox()}>
          <span className="glyphicon glyphicon-user" aria-hidden="true"></span>
        </div>
        <div id="login-menu" className={'login-menu '+ (this.props.isLoginBoxOpen ? 'open' : '')}>
          <p className="login-heading">Login</p>
          {(this.props.loginState && !this.props.loginState.loggedIn) ?
            <div id="login-content" className="login-content">
              <div className="login-input">
              	<label>User Name: <input id="username" className={(this.state.loginFailed) ? 'error' : ''} type="text" name="username" onKeyPress={(evt)=>this.keyPress(evt)}/> </label>
              </div>
              <div className="login-input">
              	<label>Password: <input id="password" className={(this.state.loginFailed) ? 'error' : ''} type="password" name="password" onKeyPress={(evt)=>this.keyPress(evt)}/> </label>
              </div>
              <div className="login-input submit">
                <span className="glyphicon glyphicon-circle-arrow-right" onClick={()=>this.login()}></span>
              </div>
              {(this.state.loadingSpinner) ?
                <div className="login-input login-loading-spinner">
                  <span className="glyphicon glyphicon glyphicon-refresh"></span>
                </div>
                :''}
            </div>
          :
            <div className="login-content--logged-in">
              <h3>Welcome!</h3>
              <div className="logout" onClick={() => this.logout()}>
                <h4 className="logout-heading">Logout</h4>
                <span className="glyphicon glyphicon glyphicon-off logout-icon"></span>
              </div>
            </div>
          }
        </div>
      </div>

  );
  }
}
export default connect((state) => ({
  loginState: state.login.loginState,
  intervalId:state.users.intervalId,
  disconnectStompClient: state.websocket.functionDisconnectStompClient,
}), (dispatch) => ({
  saveCredentials : (loginState) => dispatch(loginActions.saveCredentials(loginState)),
  openLoginBox : (functionOpenLoginBox) => dispatch(loginActions.openLoginBox(functionOpenLoginBox))
}))(Login)
