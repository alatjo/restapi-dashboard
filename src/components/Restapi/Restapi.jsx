import React, { Component } from 'react';
import {connect} from 'react-redux';
import './Restapi.css';
import * as usersActions from '../../actions/users-actions';
import * as mapActions from '../../actions/map-actions';
import LoginRequest from '../LoginRequest/LoginRequest';
import * as Config from '../../resources/config';
import * as Constants from '../../resources/constants';
import _ from 'lodash';

class Restapi extends Component {

  constructor(props) {
    super(props);
    this.waitingMessage = Constants.USERS_NOT_FOUND;
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.loginState && nextProps.loginState.credentials && !_.isEqual(this.props.loginState, nextProps.loginState)) {
      this.getUsers(nextProps.loginState.credentials);
      const id = this.triggerGetUsersIntervall(nextProps.loginState.credentials);
      this.props.intervalId(id);
    }
  }

  triggerGetUsersIntervall(credentials) {
    return (
      setInterval(() => {
        this.getUsers(credentials);
      }, 60000)
    );
  }

  getUsers(credentials) {
    this.waitingMessage = Constants.USERS_LOADING;
    fetch(Config.backend.apiUrl, {
      headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Basic ' + credentials
      }
    })
    .then(results => results.json())
    .then((users) => {
      if(users.length === 0) this.waitingMessage = Constants.USERS_NOT_FOUND
      this.props.addUsers(users);
    })
    .catch(() => this.waitingMessage = Constants.USERS_ERROR)
  }

  clickedRow(user){
    this.props.clickedUserRow(user);
  }

  componentWillMount(){
    if(this.props.loginState && this.props.loginState.loggedIn) {
      if(this.props.intervalId !== null) {
        clearInterval(this.props.intervalId);
      }
      const id = this.triggerGetUsersIntervall(this.props.loginState.credentials);
      this.props.intervalId(id);
    }
  }

  render() {
    return(
      <div>
        {(this.props.loginState && this.props.loginState.loggedIn) ?
          <div id="table" className="table-responsive">
          <table className="table table-hover">
            <thead>
              <tr>
                <th>Username</th>
                <th>Company</th>
                <th>Email</th>
                <th>Location</th>
              </tr>
            </thead>
            <tbody>
            {(this.props.users.length && this.props.users.length > 0) ?
              this.props.users.map(
              user => <tr className="user-row" key={user.id} style={{"textAlign": "left"}} onClick={() => this.clickedRow(user)}><td>{user.username}</td>
                <td>{user.company}</td>
                <td>{user.email}</td>
                {(user.address) ? <td>{user.address}, {user.postalCode} {user.city}</td> : <td>Not found</td>}
              </tr>)
              : <tr><td /><td style={{"textAlign": "left"}}>{this.waitingMessage}</td><td /></tr>
            }
            </tbody>
          </table>
        </div>
      :
        <LoginRequest />}
      </div>
    )
  }
}

export default connect((state) => ({
  users : state.users.users,
  loginState: state.login.loginState,
  intervalId: state.users.intervalId
}), (dispatch) => ({
  addUsers : (users) => dispatch(usersActions.addUsers(users)),
  clickedUserRow: (clickedUser) => dispatch(mapActions.clickedUserRow(clickedUser)),
  intervalId: (id) => dispatch(usersActions.intervalId(id))
}))(Restapi)
