import React, { Component } from 'react';
import {connect} from 'react-redux';
import MyStomp from '../../ext/stomp.js';
import MySock from 'sockjs-client';
import * as Config from '../../resources/config';
import * as notificationsActions from '../../actions/notifications-actions';
import * as messageActions from '../../actions/message-actions';
import * as webSocketActions from '../../actions/websocket-actions';
import * as loginActions from '../../actions/login-actions';
import NotificationBell from '../NotificationBell/NotificationBell';
import Messages from '../Messages/Messages';
import Login from '../Login/Login';
import '../NotificationBell/NotificationBell.css';
import '../Messages/Messages.css';
import './NotificationHeader.css';
import _ from 'lodash';

class NotificationHeader extends Component {
  constructor(props) {
    super(props);
    this.toggleMessageBox = this.toggleMessageBox.bind(this);
    this.toggleNotificationBox = this.toggleNotificationBox.bind(this);
    this.toggleLoginBox = this.toggleLoginBox.bind(this);
    this.sendMessage = this.sendMessage.bind(this);
    this.connectStompClient = this.connectStompClient.bind(this);
    this.disconnectStompClient = this.disconnectStompClient.bind(this);
    this.props.sendMessage(this.sendMessage);
    this.props.connectStompClient(this.connectStompClient);
    this.props.disconnectStompClient(this.disconnectStompClient);
  }

  componentDidMount() {
    if(this.props.loginState && this.props.loginState.loggedIn) {
      this.connectStompClient();
    }
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.loginState && nextProps.loginState.loggedIn && !_.isEqual(this.props.loginState, nextProps.loginState)) {
      this.connectStompClient();
    }
  }

  toggleMessageBox() {
    if(this.props.isNotificationBoxOpen){
      this.props.notificationBoxOpen(false);
    }
    if(this.props.isLoginBoxOpen){
      this.props.loginBoxOpen(false);
    }
    this.props.messageBoxOpen(!this.props.isMessageBoxOpen)
  }

  toggleNotificationBox() {
    if(this.props.isMessageBoxOpen){
      this.props.messageBoxOpen(false);
    }
    if(this.props.isLoginBoxOpen){
      this.props.loginBoxOpen(false);
    }
    this.props.notificationBoxOpen(!this.props.isNotificationBoxOpen)
  }

  toggleLoginBox() {
    if(this.props.isMessageBoxOpen){
      this.props.messageBoxOpen(false);
    }
    if(this.props.isNotificationBoxOpen){
      this.props.notificationBoxOpen(false);
    }
    this.props.loginBoxOpen(!this.props.isLoginBoxOpen)
  }

  connectStompClient() {
    this.socket = MySock(Config.backend.websocketUrl);
    this.stompClient = MyStomp.Stomp.over(this.socket);
    const headers = {
      login: 'rest',
      passcode: 'pw'
    }
    this.stompClient.connect(headers, (frame) => {
      this.setConnected(true);
        console.log('Connected: ' + frame);
        this.stompClient.subscribe('/topic/messages', (messageOutput) => {
          let chatMessagesArray = this.props.chatMessagesArray.slice();
          chatMessagesArray.push(JSON.parse(messageOutput.body));
          this.props.chatMessages(chatMessagesArray);
        });
    });
  }

  setConnected(connected) {
    console.log('setConnected: '+connected);
    this.props.webSocketConnection(connected);
  }

  disconnectStompClient() {
    if(this.stompClient != null) {
        this.stompClient.disconnect();
    }
    this.setConnected(false);
    console.log("Disconnected");
  }

  sendMessage(text) {
    this.stompClient.send("/app/chat", {}, JSON.stringify({'from':'You', 'text':text}));
  }

  calculateUnreadMessages(){
    return (
      this.props.chatMessagesArray.filter(message => (message.from !== 'You' && message.read === false)).length
    )
  }

  render() {
    return (
      <div className="NotificationHeader">
        {(this.props.loginState && this.props.loginState.loggedIn) ?
          <div className="notification-content">
            <Messages
            isMessageBoxOpen={this.props.isMessageBoxOpen}
            toggleMessageBox={this.toggleMessageBox}
            unreadMessages={this.calculateUnreadMessages()}
          />
          <NotificationBell
            users={this.props.users}
            isNotificationBoxOpen={this.props.isNotificationBoxOpen}
            toggleNotificationBox={this.toggleNotificationBox}
          />
          </div>
          : ""
        }
        <Login
          isLoginBoxOpen={this.props.isLoginBoxOpen}
          toggleLoginBox={this.toggleLoginBox}
        />
      </div>
    );
  }
}
export default connect((state) => ({
  users : state.users.users,
  isNotificationBoxOpen: state.notifications,
  isMessageBoxOpen: state.messages,
  isLoginBoxOpen: state.login.showLoginBox,
  chatMessagesArray: state.websocket.showChatMessages,
  loginState: state.login.loginState
}), (dispatch) => ({
  notificationBoxOpen: (showNotificationBox) => dispatch(notificationsActions.notificationBoxOpen(showNotificationBox)),
  messageBoxOpen: (showMessageBoxOpen) => dispatch(messageActions.messageBoxOpen(showMessageBoxOpen)),
  loginBoxOpen: (showLoginBoxOpen) => dispatch(loginActions.loginBoxOpen(showLoginBoxOpen)),
  webSocketConnection: (showWebSocketConnection) => dispatch(webSocketActions.connectionStatus(showWebSocketConnection)),
  chatMessages: (showChatMessages) => dispatch(webSocketActions.chatMessages(showChatMessages)),
  sendMessage: (functionSendMessage) => dispatch(webSocketActions.sendMessage(functionSendMessage)),
  connectStompClient: (functionConnectStompClient) => dispatch(webSocketActions.connectStompClient(functionConnectStompClient)),
  disconnectStompClient: (functionDisconnectStompClient) => dispatch(webSocketActions.disconnectStompClient(functionDisconnectStompClient))
}))(NotificationHeader)
