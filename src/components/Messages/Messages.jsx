import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import './Messages.css';
import * as constants from '../../resources/constants';

export default class Messages extends Component {

  constructor(props) {
    super(props);
    this.state = {
      envelopeColor: constants.ENVELOPE_WHITE,
      unreadMessages: this.props.unreadMessages
    }
  }

  toggleMessageBox() {
    this.props.toggleMessageBox();
    this.setState({
      envelopeColor: constants.ENVELOPE_WHITE
    });
  }

  markMessagesAsRead(){
    this.setState({unreadMessages: 0})
  }

  componentDidUpdate(prevProps){
    if(prevProps.unreadMessages !== this.props.unreadMessages){
      this.setState({unreadMessages: this.props.unreadMessages})
    };
  }

  render() {
    return (
      <div className="Messages">
        <a className="Messages-icon white" onClick={() => this.toggleMessageBox()}>
          <span className="glyphicon glyphicon-envelope" aria-hidden="true"></span>
        </a>
        <div className={'MessageBox' + (this.props.isMessageBoxOpen ? " show" : "")}>
          <div className="MessageBox-message">
            <Link to="/messages" onClick={() => this.toggleMessageBox()}><p className="MessageBox-message-heading">Messages</p></Link>
            {(this.state.unreadMessages > 0)
              ? <p className="MessageBox-message-text">You have {this.state.unreadMessages} unread messages
                <a className="MessageBox-message-remove" onClick={() => this.markMessagesAsRead()}>
                  <span className="glyphicon glyphicon-remove" aria-hidden="true"></span>
                </a>
              </p>
              : <p className="MessageBox-message-text">No new messages...</p>
            }
          </div>
        </div>
        {(this.state.unreadMessages > 0)
          ? <div className="MessageBox-message-unread">{this.state.unreadMessages}</div>
          : ""
        }
      </div>
    );
  }
}
