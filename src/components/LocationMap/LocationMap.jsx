import React, { Component } from 'react';
import {connect} from 'react-redux';
import * as mapActions from '../../actions/map-actions';
import { Map, TileLayer, Marker, Popup } from 'react-leaflet';
import L from 'leaflet';
import { OpenStreetMapProvider } from 'leaflet-geosearch';
import 'leaflet/dist/leaflet.css';
import './LocationMap.css';
import _ from 'lodash';
delete L.Icon.Default.prototype._getIconUrl;

L.Icon.Default.mergeOptions({
  iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
  iconUrl: require('leaflet/dist/images/marker-icon.png'),
  shadowUrl: require('leaflet/dist/images/marker-shadow.png'),
});

class LocationMap extends Component {
  constructor(props){
    super(props);
    this.provider = new OpenStreetMapProvider();
    this.state = {
      center: {lng:60.1713198,lat:24.9414566,zoom:12},
      userLocations: props.userLocations,
      users: 0
    }
  }

  calculateHeight() {
    const footer = document.getElementById('footer');
    const map = document.getElementById('map');
    if(footer && map){
      const mapHeight = window.innerHeight - map.offsetTop - footer.offsetHeight;
      document.getElementById('map').style.height = mapHeight+"px";
    }
  }

  getUserLocations(props) {
    let fetchedLocations = [];
    _.each(props.users, (user) => {
      this.provider
      .search({ query: "".concat(user.address, ", ", user.postalCode, " ", user.city) })
      .then((result) => {
        let location = {};
        location.id = user.id;
        location.desc = result[0].label;
        location.coord = [parseFloat(result[0].y), parseFloat(result[0].x)];
        location.zoom = 13;
        fetchedLocations.push(location);
        this.setState({userLocations: fetchedLocations});
      })
      .catch((err) => console.log(err));
    });
    this.props.addUserLocations(fetchedLocations);
    this.setState({users: props.users.length});
  }

  getPosition(){
    const defaultPosition = [this.state.center.lng, this.state.center.lat]
    if(this.props.clickedUser.id) {
      const foundUser = _.find(this.state.userLocations, (user) => user.id === this.props.clickedUser.id);
      if(foundUser) {
          return foundUser.coord;
      } else {
          return defaultPosition;
      }
    }else {
      return defaultPosition;
    }
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.users.length === 0) {
      this.setState({userLocations: {}});
    }
    if(!_.isEqual(this.props.users, nextProps.users)) {
      this.getUserLocations(nextProps)
      return;
    }
    if(this.state.users !== nextProps.users.length) {
      this.getUserLocations(nextProps)
    }
  }

  componentDidMount(){
    this.calculateHeight();
    window.addEventListener('resize', () => {
      this.calculateHeight();
    }, true);
  }

  componentDidUpdate(){
    this.calculateHeight();
  }

  render() {
    const position = this.getPosition();
    return (
      <div className="container">
        {(this.props.loginState && this.props.loginState.loggedIn && this.state.userLocations.length > 0) ?
          <Map id="map" center={position} zoom={this.state.center.zoom} animate={true}>
          <TileLayer
            attribution="&amp;copy <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
            url="http://{s}.tile.osm.org/{z}/{x}/{y}.png"
          />
          {(this.state.userLocations && this.state.userLocations.length > 0) ?
            this.state.userLocations.map(userlocation =>
              <Marker key={userlocation.id} position={userlocation.coord}>
                <Popup>
                  <span>{userlocation.desc}</span>
                </Popup>
              </Marker>
            )
          : ""}
        </Map>
        : ""}
      </div>
    )
  }
}
export default connect((state) => ({
  users: state.users.users,
  userLocations: state.userLocations.fetchedLocations,
  clickedUser: state.userLocations.clickedUser,
  loginState: state.login.loginState
}), (dispatch) => ({
  addUserLocations: (fetchedLocations) => dispatch(mapActions.addUserLocations(fetchedLocations))
}))(LocationMap)
