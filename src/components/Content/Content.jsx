import React, { Component } from 'react';
import {connect} from 'react-redux';
import Restapi from '../Restapi/Restapi';
import LocationMap from '../LocationMap/LocationMap';

class Content extends Component {
  render() {
    return (
      <div>
        <Restapi />
        <LocationMap users={this.props.users}/>
      </div>
    );
  }
}
export default connect((state) => ({users : state.users}))(Content)
