import React, { Component } from 'react';
import {connect} from 'react-redux';
import './LoginRequest.css'

class LoginRequest extends Component {
  render(){
    return (
      <div className="login-reguest-container">
        <div className="login-request-icon">
          <div className="login-request-circle" onClick={() => this.props.openLoginBox()}>
            <span className="glyphicon glyphicon-user glyphicon-user--top" aria-hidden="true"></span>
          </div>
        </div>
        <h3 className="login-request-heading">Hello Stranger! Please login</h3>
      </div>
    );
  }
}
export default connect((state) => ({
  openLoginBox: state.login.functionOpenMenubox
}))(LoginRequest)
