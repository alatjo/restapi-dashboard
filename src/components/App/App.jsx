import React, { Component } from 'react';
import {BrowserRouter as Router, Route, Link} from 'react-router-dom';
import Content from '../Content/Content';
import MessagePage from '../MessagePage/MessagePage';
import NotificationHeader from '../NotificationHeader/NotificationHeader';
import logo from '../../images/logo.svg';
import './App.css';

export default class App extends Component {
  render() {
    return (
      <div className="App">
        <Router>
          <div className="App-container">
            <div id="header" className="App-header">
              <img src={logo} className="App-logo" alt="logo" />
              <Link to="/"><h3 className="App-heading">Dashboard</h3></Link>
              <NotificationHeader />
            </div>
            <Route exact path="/" component={Content} />
            <Route path="/messages" component={MessagePage}/>
            <div id="footer" className="footer"><p>© Joni Alatalo</p></div>
          </div>
        </Router>
      </div>
    );
  }
}
