import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import './NotificationBell.css';
import * as constants from '../../resources/constants';

export default class NotificationBell extends Component {

  constructor(props) {
    super(props);
    this.state = {
      bellColor: constants.NOTIFICATION_BELL_WHITE,
      message: constants.NOTIFICATION_DEFAULT_MESSAGE,
      bellAnimation: false
    };
  }

  componentDidUpdate(prevProps) {
    if(prevProps.users.length !== this.props.users.length){
      if(prevProps.users.length > this.props.users.length) {
        this.setState({
          bellColor: constants.NOTIFICATION_BELL_YELLOW,
          previousUsers: this.props.users,
          message: (prevProps.users.length - this.props.users.length) + constants.NOTIFICATION_REMOVE_MESSAGE,
          bellAnimation: true
        });
      } else if(prevProps.users.length < this.props.users.length) {
        this.setState({
          bellColor: constants.NOTIFICATION_BELL_YELLOW,
          previousUsers: this.props.users,
          message: (this.props.users.length - prevProps.users.length) + constants.NOTIFICATION_ADD_MESSAGE,
          bellAnimation: true
        });
      }
    }
    this.animateBell(this.state.bellAnimation);
  }

  animateBell(animate){
    let bell = document.getElementById("bell");
    if(animate) {
      bell.style.animationName = "myAnimation";
      bell.style.animationPlayState = "running";
    } else {
      //workaround to set bell to its default place
      bell.style.animationName = "stoppedAnimation";
    }
  }

  removeNotificationMessage() {
    this.setState({
      message : constants.NOTIFICATION_DEFAULT_MESSAGE
    });
  }

  toggleNotificationBox() {
    this.props.toggleNotificationBox();
    this.setState({
      bellColor: constants.NOTIFICATION_BELL_WHITE,
      bellAnimation: false
    });
  }

  render() {
    return (
      <div className="Notifications">
        <a className={'Notifications-bell-color' + (this.state.bellColor === constants.NOTIFICATION_BELL_YELLOW ? " yellow" : " white")}
          onClick={() => this.toggleNotificationBox()}>
          <span id="bell" className="Notifications-bell-animate glyphicon glyphicon-bell" aria-hidden="true"></span>
        </a>
        <div className={'Notification-box' + (this.props.isNotificationBoxOpen ? " show" : "")}>
          <div className="Notification-message">
            <Link to="/"><p className="Notification-message-heading">Notifications</p></Link>
            <p className="Notification-message-text">{this.state.message}
              <a className={'Notification-message-remove' + (this.state.message !== constants.NOTIFICATION_DEFAULT_MESSAGE ? " show" : "")}
                onClick={() => this.removeNotificationMessage()}>
                <span className="glyphicon glyphicon-remove" aria-hidden="true"></span>
              </a>
            </p>
          </div>
        </div>
      </div>
    );
  }
}
