import React, { Component } from 'react';
import {connect} from 'react-redux';
import * as webSocketActions from '../../actions/websocket-actions';
import './MessagePage.css';
import LoginRequest from '../LoginRequest/LoginRequest';
import _ from 'lodash';

class MessagePage extends Component {

  constructor(){
    super();
    this.rotate = 360;
  }

  keyPress(e) {
    if(e && e.key === 'Enter') {
      this.sendMessage();
      e.target.value = "";
    }
  }

  handleScroll(element){
    element.scrollTop = element.scrollHeight;
  }

  markMessagesAsRead(messages){
    _.each(messages, (message) => message.read = true);
    this.props.chatMessages(messages);
  }

  sendMessage() {
    this.props.sendMessage(document.getElementById("message-input-text").value);
  }

  componentDidMount() {
    if(document.getElementById("chatBox")){
      this.handleScroll(document.getElementById("chatBox"));
    }
    //set all messages as read when coming to site
    this.markMessagesAsRead(this.props.showChatMessages.slice());
  }

  onClickRefresh(e){
    e.target.style.transform = "rotate("+this.rotate+"deg)";
    this.props.connectStompClient();
    this.rotate += 360;
  }

  componentDidUpdate() {
    if(document.getElementById("chatBox")){
      this.handleScroll(document.getElementById("chatBox"));
    }

    //set all messages as read while user is at the site
    if(_.filter(this.props.showChatMessages, (message) => {
        return (message.read === false && message.from !== 'You')
      }
    ).length > 0){
      this.markMessagesAsRead(this.props.showChatMessages.slice());
    }
  }

  render() {
    return (
      <div>
        {(this.props.loginState && this.props.loginState.loggedIn) ?
        <div>
          <div className="message-headline">
            <h3>Messages</h3>
              {this.props.webSocketConnectionStatus
              ? <span className="message-headline-icon message-headline-icon--ok glyphicon glyphicon-ok-sign"></span>
              :
              <div style={{"alignSelf" : "center"}}>
                <span className="message-headline-icon message-headline-icon--fail glyphicon glyphicon-remove-sign"></span>
                <span onClick={(element) => this.onClickRefresh(element)} className="message-headline-refresh-icon message-headline-refresh-icon--rotate glyphicon glyphicon-refresh"></span>
              </div>}
          </div>
          <div className="message-headline-separator"></div>
            <div id="conversationDiv">
  		        <div id="chatBox" className="message">
                {(this.props.showChatMessages && this.props.showChatMessages.length > 0) ?
                  this.props.showChatMessages.map(
                    chatMessage => <p key={chatMessage.key} className={'message-' + (chatMessage.from === 'You' ? "you" : "admin")}>
                      {chatMessage.from}: {chatMessage.text} {chatMessage.time}
                    </p>
                  )
                  : <p>No new messages...</p>
                }
              </div>
              <div className="message-inputbox">
                <input id="message-input-text" className="message-input" type="text" aria-describedby="sizing-addon1" onKeyPress={(evt)=>this.keyPress(evt)} placeholder="Write a message..."/>
                <span className="message-send glyphicon glyphicon-circle-arrow-right" onClick={()=>this.sendMessage()}></span>
              </div>
            </div>
        </div>
        : <LoginRequest />}
      </div>
    );
  }
}

export default connect((state) => ({
  webSocketConnectionStatus : state.websocket.showWebSocketConnection,
  showChatMessages: state.websocket.showChatMessages,
  sendMessage: state.websocket.functionSendMessage,
  connectStompClient: state.websocket.functionConnectStompClient,
  loginState: state.login.loginState,
}), (dispatch) => ({
  chatMessages: (showChatMessages) => dispatch(webSocketActions.chatMessages(showChatMessages)),
}))(MessagePage)
