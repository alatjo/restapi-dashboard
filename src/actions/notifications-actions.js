import * as types from './action-types';

export const notificationBoxOpen = (showNotificationBox) => {
  return {
    type: types.NOTIFICATION_BOX_OPEN,
    showNotificationBox
  };
}
