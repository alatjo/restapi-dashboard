import * as types from './action-types';

export const connectionStatus = (showWebSocketConnection) => {
  return {
    type: types.WEBSOCKET_CONNECTION_STATUS,
    showWebSocketConnection
  };
}
export const chatMessages = (showChatMessages) => {
  return {
    type: types.WEBSOCKET_CHAT_MESSAGES,
    showChatMessages
  };
}
export const sendMessage = (functionSendMessage) => {
  return {
    type: types.WEBSOCKET_FUNCTION_SEND_MESSAGE,
    functionSendMessage
  };
}
export const connectStompClient = (functionConnectStompClient) => {
  return {
    type: types.WEBSOCKET_FUNCTION_CONNECT_STOMP_CLIENT,
    functionConnectStompClient
  };
}
export const disconnectStompClient = (functionDisconnectStompClient) => {
  return {
    type: types.WEBSOCKET_FUNCTION_DISCONNECT_STOMP_CLIENT,
    functionDisconnectStompClient
  };
}
