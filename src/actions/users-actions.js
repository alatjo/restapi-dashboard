import * as types from './action-types';

export const addUsers = (users) => {
  return {
    type: types.ADD_USERS,
    users
  };
}
export const intervalId = (id) => {
  return {
    type: types.ADD_INTERVAL_ID,
    id
  };
}
