import * as types from './action-types';

export const messageBoxOpen = (showMessageBox) => {
  return {
    type: types.MESSAGE_BOX_OPEN,
    showMessageBox
  };
}
