import * as types from './action-types';

export const saveCredentials = (loginState) => {
  return {
    type: types.SAVE_CREDENTIALS,
    loginState
  };
}
export const loginBoxOpen = (showLoginBox) => {
  return {
    type: types.OPEN_MENUBOX,
    showLoginBox
  };
}
export const openLoginBox = (functionOpenLoginBox) => {
  return {
    type: types.OPEN_MENUBOX_FUNCTION,
    functionOpenLoginBox
  };
}
