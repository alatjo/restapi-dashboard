import * as types from './action-types';

export const addUserLocations = (fetchedLocations) => {
  return {
    type: types.ADD_USER_LOCATIONS,
    fetchedLocations
  };
}
export const clickedUserRow = (clickedUser) => {
  return {
    type: types.CLICKED_USER,
    clickedUser
  };
}
