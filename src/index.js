import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { loadState, saveState } from './localStorage';
import rootReducer from './reducers'
import throttle from 'lodash/throttle';
import App from './components/App/App';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';
import './styles/index.css';

const persistedState = loadState();
const store = createStore(rootReducer,persistedState);


store.subscribe(throttle(() => {
  saveState(store.getState());
}), 1000);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
