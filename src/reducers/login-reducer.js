import * as types from '../actions/action-types';

export default (state = {loginState:{}, showLoginBox:false, functionOpenMenubox:{}}, action) => {
  switch (action.type) {
    case types.SAVE_CREDENTIALS:
      return {
        ...state,
        loginState: action.loginState.loginState
      }
    case types.OPEN_MENUBOX:
      return {
        ...state,
        showLoginBox: action.showLoginBox
      }
    case types.OPEN_MENUBOX_FUNCTION:
      return {
        ...state,
        functionOpenMenubox: action.functionOpenLoginBox
      }
    default:
      return state;
  }
};
