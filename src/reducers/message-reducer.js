import * as types from '../actions/action-types';

export default (state = false, action) => {
  switch (action.type) {
    case types.MESSAGE_BOX_OPEN:
      return action.showMessageBox;
    default:
      return state;
  }
};
