import * as types from '../actions/action-types';

export default (state = {fetchedLocations:[],clickedUser:{}}, action) => {
  switch (action.type) {
    case types.ADD_USER_LOCATIONS:
      return {
        ...state,
        fetchedLocations: action.fetchedLocations
      }
    case types.CLICKED_USER:
      return {
        ...state,
        clickedUser: action.clickedUser
      }
    default:
      return state;
  }
};
