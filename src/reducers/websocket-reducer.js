import * as types from '../actions/action-types';

export default (state = {showWebSocketConnection: false, showChatMessages: [], functionSendMessage: {}, functionConnectStompClient: {}, functionDisconnectStompClient: {}}, action) => {
  switch (action.type) {
    case types.WEBSOCKET_CONNECTION_STATUS:
      return {
        ...state,
        showWebSocketConnection: action.showWebSocketConnection
      }
    case types.WEBSOCKET_CHAT_MESSAGES:
      return {
        ...state,
        showChatMessages: action.showChatMessages
      }
    case types.WEBSOCKET_FUNCTION_SEND_MESSAGE:
      return {
        ...state,
        functionSendMessage: action.functionSendMessage
      }
    case types.WEBSOCKET_FUNCTION_CONNECT_STOMP_CLIENT:
      return {
        ...state,
        functionConnectStompClient: action.functionConnectStompClient
      }
    case types.WEBSOCKET_FUNCTION_DISCONNECT_STOMP_CLIENT:
      return {
        ...state,
        functionDisconnectStompClient: action.functionDisconnectStompClient
      }
    default:
      return state;
  }
};
