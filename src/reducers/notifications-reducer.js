import * as types from '../actions/action-types';

export default (state = false, action) => {
  switch (action.type) {
    case types.NOTIFICATION_BOX_OPEN:
      return action.showNotificationBox
    default:
      return state;
  }
};
