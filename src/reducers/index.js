import { combineReducers } from 'redux';
import users from './users-reducer';
import notifications from './notifications-reducer';
import messages from './message-reducer';
import websocket from './websocket-reducer';
import userLocations from './map-reducer';
import login from './login-reducer';

const rootReducer = combineReducers({
  users,
  notifications,
  messages,
  websocket,
  userLocations,
  login
})

export default rootReducer;
