import * as types from '../actions/action-types';

export default (state = {users:[],intervalId:{}}, action) => {
  switch (action.type) {
    case types.ADD_USERS:
      return {
        ...state,
        users: action.users
      }
    case types.ADD_INTERVAL_ID:
      return {
        ...state,
        intervalId: action.id
      }
    default:
      return state;
  }
};
