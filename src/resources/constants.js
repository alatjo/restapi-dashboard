//NOTIFICATIONS
export const NOTIFICATION_REMOVE_MESSAGE = ' amount of users has been deleted from the system';
export const NOTIFICATION_ADD_MESSAGE = ' new user has been added to the system';
export const NOTIFICATION_DEFAULT_MESSAGE = 'no messages';

//BELL COLORS
export const NOTIFICATION_BELL_WHITE = 'white';
export const NOTIFICATION_BELL_YELLOW = 'yellow';

//ENVELOPE COLORS
export const ENVELOPE_WHITE = 'white';
export const ENVELOPE_YELLOW = 'yellow';

//RESTAPI MESSAGE
export const USERS_NOT_FOUND = 'Not found...';
export const USERS_LOADING = 'Loading...';
export const USERS_ERROR = 'Error occurred...'
