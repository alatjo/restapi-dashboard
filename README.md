# README #
# Dashboard application created with React/Redux framework  
![restapi-dashboar-screenshot.png](https://bitbucket.org/repo/R99Keyp/images/3923895802-restapi-dashboar-screenshot.png)
>Description:
>
>Application provide dashboard for back-end data through rest api layer and websocket connection.
>> Features:  
>1.Login  
>2.Show userdata  
>3.Show user/companies in map  
>4.Chat between client & Admin (backend)  
>5.Dynamic notifications of data changes and new messages via header icons
>
>>Tech:  
>Used libraries:  
>1.React (create-react-app)  
>2.Redux  
>3.SockJS/StompClient  
>4.Leaflet (Map)  
>5.Bootstrap

#Prerequisities:#
Docker Engine  

#Info  
Backend (http://localhost/index)  
Credentials:  
username: user  
password: pw

Dashboard (http://localhost:3000)  
Credentials:  
username: rest  
password: pw

>Via Backend: You can add new users with location info and login in chat from messages page.  
Via Dashboard: You can check places from the map and discuss with "admin"/backend user in real time 

#Start
1. Clone backend from https://alatjo@bitbucket.org/alatjo/restapi.git  
2. Go to the root folder of restapi. Build jar and create local docker image  

```
#!shell
mvn clean package
docker build -t backend:latest .
```
3. Go to the root folder of restapi-dashboard and create docker image
```
#!shell
docker build -t dashboard:latest .
```  
4. Now you have both docker images created so you can run following docker-compose command under restapi-dashboard folder  
```
#!shell

docker-compose up (-d if you want to run it detached)
```

#Alternative start setup:  
You can compose backend infra separately from dashboard.  
Go to the restapi root folder and run following commands: 
```
#!shell
mvn clean package
docker-compose up
```
after that you can go to the restapi-dashboard project root folder and run dashboard via command:     
```
#!shell
yarn start
```

#Stop
```
#!shell

docker-compose down
```

#Known Issues  
1. Map markers will be rendered before map size is calculated so there is small mismatch. Zoom in & Zoom out map will set market accordingly